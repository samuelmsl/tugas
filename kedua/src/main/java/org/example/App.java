package org.example;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println("Hello World!");

        // ini string

        ArrayList<String> collection1 = new ArrayList<>();
        collection1.add("Risa");
        collection1.add("Reza");
        collection1.add("Liana");

        // ini int

        ArrayList<Integer> collection2 = new ArrayList<>();
        collection2.add(1);
        collection2.add(2);
        collection2.add(3);

        // ini barang

        ArrayList<String> goods = new ArrayList<>();
        goods.add("Indomie");
        goods.add("Kapal Api");
        goods.add("Popok Bayi");
        goods.add("Shampoo");
        goods.add("Sabun Mandi");


        ArrayList<String> goodsNew = new ArrayList<>();
        goods.add("Indomie");
        goods.add("Kapal Api");
        goods.add("Baygon");


        // ini contoh for

//        for(String name : collection1) {
//            System.out.println("Nama : " + name);
//        }

//        Iterator itrCollection1 = collection2.iterator(); // ini menggunakan iterator
//        while (itrCollection1.hasNext()) {
//            System.out.println("Nama : " + itrCollection1.next());
//        }
//
//        System.out.println("Panjang : " + collection2.size());
//
//        collection2.add(1, 10);
//
//        System.out.println("Panjang sekarang : " + collection2.size());
//
//        collection2.remove(1);
//
//        System.out.println("Panjang setelah di hapus " + collection2.size());


        // size() panjang ArrayList
        // clear() membersihkan collection
        // Iterator digunakan bersamaan dengan hasNext() dan next();


        // ====== PERBANDINGAN COLLECTION =======

        // equals() sama dengan
        // containsAll() perbandingan anatara yang ada di dalam situ
        // Contains(parameter) perbandingan ada satu
        // retainAll() menghapus semua kecuali yang sama
        // get(parameter) untuk ambil ArrayList
        // ada jenis add beda, yang urut dan tidak urut add()


        // =======COMPARE DATA=======
//
//        goods.addAll(goodsNew);
//        System.out.println(goods.size());


//             String initGoods = goods.containsAll(goodsNew);
//             String initGoodNew = goods.addAll(goodsNew);


//            goods.remove(initGoods);
//            goods.add(initGoodNew);


        System.out.println("=========TUGAS ARRAY=========");


//
//        ArrayList<Integer> allList = new ArrayList<>();

        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        System.out.println("array list1: " + list1);

        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(5);


        System.out.println("array list2: " + list2);


        ArrayList<Integer> retain = new ArrayList<>(list1);
        retain.retainAll(list2);

//        System.out.println(intersection);

            list2.removeAll(retain);
            list1.removeAll(retain);
            list1.addAll(list2);

            System.out.println("Array list1 setelah di ubah: " + list1);


        }
    }

